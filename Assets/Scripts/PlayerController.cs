﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField]
    private float playerSpeed = 5;
    [SerializeField]
    private float jumpPower = 5;
    private float jumpTime = 0.3f;
    private float jumpTimer;
    private bool jumping;
    [SerializeField]
    private float stunnedTime = 1;
    private float stunnedTimer;
    [SerializeField]
    private float fadeTime = 3;
    private bool faded;
    [SerializeField]
    private string playerLayer = "Player";
    [SerializeField]
    private string enemyLayer = "MovingUnit";
    [SerializeField]
    private GameObject playerSkin;
    [SerializeField]
    private Material fadeMaterial;
    private Material startMaterial;
    [SerializeField]
    private float climbSpeed = 3;
    [SerializeField]
    private bool ladderStick = true;
    private bool stuckToLadder;
    [SerializeField]
    private Transform circlecastDown;
    [SerializeField]
    private float circlecastDistance;
    [SerializeField]
    private LayerMask groundMask;
    [SerializeField]
    private bool doubleJump;
    [SerializeField]
    private bool dash;
    [SerializeField]
    private float dashSpeed = 5;
    [SerializeField]
    private float dashTime = 0.2f;
    private float dashTimer;
    private bool dashing;
    private bool dashInp;
    [SerializeField]
    private float dashCooldown = 3;
    private bool coolDown;
    [SerializeField]
    private bool sprint;
    [SerializeField]
    private float sprintSpeed;
    private bool sprintInp;
    [SerializeField]
    private bool crouch;
    private bool crouched;
    [SerializeField]
    private float crouchSpeed = 3;
    [SerializeField]
    private float crouchHeight = 1;
    private float startHeight;

    private Vector3 move;

    private float moveX;
    private float moveY;

    private Rigidbody2D rb;
    private CapsuleCollider2D col;
    private bool moveRight;

    private bool grounded;
    private bool onPlatform;
    private bool jump;
    private bool doubleActive;
    private bool onLadder;
    private bool stunned;

    private Player player;

    // Use this for initialization
    void Start()
    {
        //get components
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<CapsuleCollider2D>();
        player = GetComponent<Player>();

        //get height
        startHeight = col.size.y;
    }

    void Update()
    {
        CheckGround();
        GetInputs();
        CheckStunned();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        MovePlayer();
        RotatePlayer();
    }

    void GetInputs()
    {

        jump = Input.GetButtonDown("Jump");
        dashInp = Input.GetButtonDown("Fire2");

        if (Input.GetButton("Run") || Input.GetAxisRaw("XboxTriggerLeft") > 0)
            sprintInp = true;
        else
            sprintInp = false;


        if (jump)
        {
            Jump();
            jumping = true;
        }

        if (jumping)
        {
            JumpTimer();
        }

        if (dash && dashInp && !coolDown)
        {
            StartCoroutine(StartDash());
        }

        if (coolDown)
        {
            DashCoolDown();
        }
    }

    void Jump()
    {

        if (grounded || stuckToLadder)
        {


            stuckToLadder = false;
            rb.AddForce(Vector3.up * jumpPower, ForceMode2D.Impulse);

        }
        else if (doubleActive)
        {

            //sleep to reset physics for 1 frame...so jump power is consistent
            rb.Sleep();
            rb.AddForce(Vector3.up * jumpPower, ForceMode2D.Impulse);
            doubleActive = false;
        }

    }

    void JumpTimer()
    {
        jumpTimer += Time.deltaTime;
        if (jumpTimer > jumpTime)
        {
            jumping = false;
            jumpTimer = 0;
        }
    }

    IEnumerator StartDash()
    {
        dashTimer = 0;
        Vector2 pos = transform.position;
        dashing = true;
        while (dashTimer < dashTime)
        {
            dashTimer += Time.fixedDeltaTime;
            transform.position = new Vector2(transform.position.x, pos.y);
            yield return new WaitForFixedUpdate();
        }
        dashing = false;
        StartCoroutine(DashCoolDown());
    }

    IEnumerator DashCoolDown()
    {
        coolDown = true;
        yield return new WaitForSeconds(dashCooldown);
        coolDown = false;
    }

    void MovePlayer()
    {
        //get axis input
        moveX = Input.GetAxisRaw("Horizontal") * Time.fixedDeltaTime;
        moveY = Input.GetAxisRaw("Vertical") * Time.fixedDeltaTime * climbSpeed;

        //sprint player
        if (sprintInp && !dashing && sprint && !crouched)
        {
            moveX *= sprintSpeed;
        }
        else if (crouched && grounded)
        {
            moveX *= crouchSpeed;
        }
        else //normal speed
        {
            moveX *= playerSpeed;
        }

        //crouch player if input is down
        if (crouch && moveY < 0 && !stuckToLadder)
        {
            if (!crouched)
            {
                SetColliderHeight(crouchHeight);
                crouched = true;
            }
        }
        else
        {
            if (crouched)
            {
                SetColliderHeight(startHeight);
                crouched = false;
            }
        }

        //move player...reverse movement if moving a certain direction.
        if (moveRight)
            move = new Vector2(moveX, 0);
        else
            move = new Vector2(-moveX, 0);

        if (dashing)
        {
            move *= dashSpeed;
        }

        //grab ladder
        if (onLadder && moveY != 0 && moveX == 0 && !jumping)
        {
            if (ladderStick && !stuckToLadder)
                stuckToLadder = true;
            else
            {
                //constant slide down ladder
                MoveUpDownLadder();
            }

        }
        else if (stuckToLadder && onLadder && !jumping)
        {
            //sticks to ladder
            MoveUpDownLadder();
        }
        else if (!stunned)
        {
            //move player normally
            transform.Translate(move);
        }

    }

    void SetColliderHeight(float _height) // change height of player collider
    {
        col.size = new Vector2(col.size.x, _height);
        col.offset = new Vector2(col.offset.x, _height / 2);
    }

    void MoveUpDownLadder()
    {
        if (!jump)
            rb.Sleep();

        //slide down ladders faster
        if (moveY < 0)
            moveY *= 3;

        transform.Translate(new Vector2(move.x, moveY));
    }

    void RotatePlayer()
    {

        //rotate player based on movement direction
        if (moveX > 0)
        {
            transform.localEulerAngles = new Vector3(0, 0, 0);
            moveRight = true;
        }
        else if (moveX < 0)
        {
            transform.localEulerAngles = new Vector3(0, 180, 0);
            moveRight = false;
        }
    }

    void CheckGround()
    {
        RaycastHit2D hit;

        //raycast down to detect ground
        hit = Physics2D.CircleCast(circlecastDown.position, col.size.x / 3, Vector2.down, circlecastDistance, groundMask);

        //stick to platform depending on ground hit
        if (hit)
        {
            if (hit.collider.tag == "Platform")
            {
                if (!onPlatform)
                {
                    Transform floor = hit.collider.transform.FindChild("Floor");
                    StickToPlatform(floor);
                }
            }
            if (!grounded)
            {
                grounded = true;
                doubleActive = false;
            }

        }
        else
        {
            if (onPlatform)
            {
                transform.SetParent(null);
                onPlatform = false;
            }
            if (grounded)
            {
                grounded = false;
                doubleActive = true;
            }

        }

        //check if below killing floor
        if (transform.position.y < GameManager.instance.GetKillHeight())
        {
            if (!player.IsDead())//kill player
                player.KillPlayer();
        }

    }

    public void Bounce(float _force, Vector2 _direction)
    {
        if (_direction.y > 0.5 || _direction.y < -0.5)
        {
            _direction = new Vector2(0, _direction.y);
        }
        else
        {
            stunned = true;
        }

        rb.Sleep();
        rb.AddForce(_direction * _force, ForceMode2D.Impulse);
    }

    void CheckStunned()
    {
        if (stunned)
        {
            stunnedTimer += Time.deltaTime;
            if (stunnedTimer > stunnedTime)
            {
                stunnedTimer = 0;
                stunned = false;
            }
        }
    }

    public IEnumerator FadePlayer()
    {
        Renderer mat = playerSkin.GetComponent<Renderer>();
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer(playerLayer), LayerMask.NameToLayer(enemyLayer), true);
        mat.material = fadeMaterial;
        faded = true;

        yield return new WaitForSeconds(fadeTime);

        mat.material = startMaterial;
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer(playerLayer), LayerMask.NameToLayer(enemyLayer), false);
        faded = false;
    }

    void StickToPlatform(Transform _floor)
    {
        if (!jump)
        {
            transform.SetParent(_floor);
            transform.position = new Vector3(transform.position.x, _floor.position.y, 0);
            onPlatform = true;
        }
    }

    public bool IsGrounded()
    {
        return grounded;
    }

    public bool IsFaded()
    {
        return faded;
    }

    public void SetOnLadder(bool _onLadder)
    {
        onLadder = _onLadder;
        if (!_onLadder)
            stuckToLadder = _onLadder;
    }


    public void SetCurSkin(GameObject _skin)
    {
        playerSkin = _skin;

        //get material
        Renderer rend = playerSkin.GetComponent<Renderer>();
        startMaterial = rend.material;
    }


}
