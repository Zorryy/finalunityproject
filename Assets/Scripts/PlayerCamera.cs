﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{

    private Transform player;
    [SerializeField]
    private float xOffset;
    [SerializeField]
    private float yOffset;

    void Update()
    {
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
            return;
        }

        Vector3 camPos = new Vector3(player.position.x + xOffset, 
            player.position.y + yOffset, transform.position.z);

        transform.position = camPos;
    }

	
}
