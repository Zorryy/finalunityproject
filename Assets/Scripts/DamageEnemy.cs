﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageEnemy : MonoBehaviour
{ 
    [SerializeField]
    private Enemy enemy;
    [SerializeField]
    private bool oneHitKill;
    [SerializeField]
    private int damage;
    [SerializeField]
    private bool trigger = true;
    [SerializeField]
    private bool bouncePlayer = true;
    [SerializeField]
    private float bounceForce = 15;

    void Start()
    {
        Collider2D col = GetComponent<Collider2D>();
        col.isTrigger = trigger;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            AddDamage();

            if (bouncePlayer)
            {
                Vector2 direction = col.contacts[0].point - new Vector2(transform.position.x, transform.position.y);
                col.gameObject.GetComponent<PlayerController>().Bounce(bounceForce, direction.normalized);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            AddDamage();
        }
    }

    void AddDamage()
    {
        int dam;
        if (oneHitKill)
            dam = enemy.GetCurHp();
        else
            dam = damage;

        enemy.DamageHp(dam);
    }
}
