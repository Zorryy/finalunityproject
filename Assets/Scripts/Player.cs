﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    [SerializeField]
    private int maxHp = 5;
    [SerializeField]
    private int maxLives = 3;
    [SerializeField]
    private Transform enemyTarget;
    [SerializeField]
    private List<GameObject> skins = new List<GameObject>();

    private int curHp;
    private int curLives;
    private int curSkinInd;

    private bool dead;

    private Vector2 startCheckPoint;
    private Vector2 curCheckPoint;

    private PlayerUI ui;
    private PlayerController pc;
    private PlayerData pd;
    private SpawnManager sm;

    // Use this for initialization
    void Start()
    {
        //get components
        ui = GameManager.instance.GetPlayerUI();
        pc = GetComponent<PlayerController>();
        pd = GameManager.instance.GetPlayerData();
        sm = GameManager.instance.GetSpawnManager();

        curLives = maxLives;
        //save lives to disc
        pd.SetCurLives(curLives);

        startCheckPoint = transform.position;
        curCheckPoint = startCheckPoint;

        //set player skin from disc
        SetPlayerSkin(pd.GetCurPlayerSkin());

        SetHealthDefaults();
    }

    public void SetPlayerSkin(int _ind)
    {
        curSkinInd = _ind;
        pd.SetCurPlayerSkin(curSkinInd);

        foreach (var skin in skins)
        {
            if (skin != skins[curSkinInd])
            {
                skin.SetActive(false);
            }
            else
            {
                skin.SetActive(true);
                if (pc)//add skin to player controller for fade effect
                {
                    pc.SetCurSkin(skin);
                }
            }
        }
    }

    public GameObject GetCurSkin()
    {
        return skins[curSkinInd];
    }

    public int GetCurSkinInd()
    {
        return curSkinInd;
    }

    public int GetCurSkinCount()
    {
        return skins.Count;
    }

    public void SetHealthDefaults()
    {
        curHp = maxHp;

        //set UI stats
        if (ui)
        {
            ui.AddHp(curHp);
            ui.SetLives(curLives);
        }

        dead = false;
        
    }

    public void AddHp(int _amount)
    {
        //only add health if not at max
        if (curHp < maxHp)
        {
            curHp += _amount;
            //update ui
            if (ui)
            {
                ui.AddHp(_amount);
            }
            
        }

    }

    public void DamageHp(int _damage)
    {
        ApplyDamage(_damage, 0, Vector2.zero);
    }

    public void DamageHp(int _damage, float _bounceForce, Vector2 _direction)
    {
        ApplyDamage(_damage, _bounceForce, _direction);
    }

    void ApplyDamage(int _damage, float _bounceForce, Vector2 _direction)
    {
        if (pc.IsFaded() || dead) //dont apply damage if fade is in effect or dead
            return;

        curHp -= _damage;

        //fade player
        StartCoroutine(pc.FadePlayer());

        //bounce player
        if (_bounceForce > 0)
        {
            BouncePlayer(_bounceForce, _direction);
        }

        //kill player if health at 0
        if (curHp <= 0)
        {
            curHp = 0;
            KillPlayer();
        }

        //update UI
        if (ui)
        {
            ui.RemoveHp(_damage);
        }

        Debug.Log("damaging player by " + _damage);
        
    }

    void BouncePlayer(float _bounceForce, Vector2 _direction)
    {
        pc.Bounce(_bounceForce, _direction.normalized);
    }

    public void KillPlayer()
    {
        dead = true;

        if (curHp > 0)
        {
            if (ui)
            {
                ui.RemoveHp(curHp);
            }
            curHp = 0;
        }
            

        curLives--;
        if (curLives > 0)
        {
            Respawn();
        }
        else
        {
            curLives = maxLives;
            Respawn();
        }
        
        Debug.Log(gameObject + " has died!");

        GetComponent<Rigidbody2D>().Sleep();

    }

    void Respawn()
    {
        StartCoroutine(sm.RespawnPlayer());
    }

    public int GetCurHp()
    {
        return curHp;
    }

    public Transform GetEnemyTarget()
    {
        return enemyTarget;
    }

    public bool IsDead()
    {
        return dead;
    }
}
