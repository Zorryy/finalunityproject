﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooter : MonoBehaviour
{

    [SerializeField]
    private GameObject projectile;
    [SerializeField]
    private float shootDelay = 1;
    [SerializeField]
    private float projectileSpeed = 3;
    [SerializeField]
    private int damage = 1;
    [SerializeField]
    private Transform nozzle;

    private float timer;

    void OnTriggerStay2D(Collider2D _col)
    {
        if (_col.tag == "Player")
        {
            LookAtPlayer(_col.GetComponent<Player>().GetEnemyTarget());
            ShootPlayer();
        }
       
    }

    void OnTriggerExit2D(Collider2D _col)
    {
        if (_col.tag == "Player")
        {
            //timer = 0;
        }
    }

    void LookAtPlayer(Transform _target)
    {
        transform.LookAt(_target);
    }

    void ShootPlayer()
    {
        timer += Time.deltaTime;
        if (timer > shootDelay)
        {
            SpawnProjectile();
            timer = 0;
        }
    }

    void SpawnProjectile()
    {
        //spawn prefab and store in a variable
        var spawn = Instantiate(projectile, nozzle.position, nozzle.rotation);

        //get projectile component
        Projectile pj = spawn.GetComponent<Projectile>();

        //set speed and damage on projectile prefab
        pj.SetSpeed(projectileSpeed);
        pj.SetDamage(damage);
    }
}
