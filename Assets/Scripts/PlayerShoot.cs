﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{

    [SerializeField]
    private float delay = 0.3f;
    private float timer;
    [SerializeField]
    private float speed = 10;
    [SerializeField]
    private int damage = 10;
    [SerializeField]
    private Transform nozzle;
    [SerializeField]
    private GameObject bulletPrefab;

    private bool fire;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        GetInputs();
    }

    void GetInputs()
    {

        if (Input.GetButton("Fire1") || Input.GetAxisRaw("XboxTriggerRight") > 0)
        {
            if (!fire)
            {
                fire = true;
                timer = delay;
            }
        }
        else if (Input.GetButtonUp("Fire1") || Input.GetAxisRaw("XboxTriggerRight") <= 0)
        {
            if (fire)
            {
                fire = false;
            }
        }

        if (fire)
        {
            Shoot();
        }
    }

    void Shoot()
    {
        timer += Time.deltaTime;
        if (timer > delay)
        {
            SpawnBullet();
            timer = 0;
        }
    }

    void SpawnBullet()
    {
        var bullet = Instantiate(bulletPrefab, nozzle.position, nozzle.rotation);
        PlayerBullet bul = bullet.GetComponent<PlayerBullet>();

        bul.SetParams(damage, speed);

    }
}
