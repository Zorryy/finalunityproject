﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour 
{

    private const string CHECKPOINTKEY = "CheckPoint";
    private const string SCENEKEY = "Scene";

    private int curCheckPoint;
    private string curScene;


    void Start()
    {
        curCheckPoint = PlayerPrefs.GetInt(CHECKPOINTKEY);
    }

    #region SCENEMANAGEMENT

    public void LoadLevel(string _name)
    {
        SceneManager.LoadScene(_name);
    }

    public void LoadLevel(int _buildInd)
    {
        SceneManager.LoadScene(_buildInd);
    }

	public void ResetCurLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public string GetCurLevelName()
    {
        return SceneManager.GetActiveScene().name;
    }

    public int GetCurLevelInd()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }

    public string GetLevelName(int _buildInd)
    {
        return SceneManager.GetSceneByBuildIndex(_buildInd).name;
    }

    public void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    #endregion

    #region SAVINGCHECKPOINTS

    public void SaveCurCheckPoint(int _ind)
    {
        curCheckPoint = _ind;
        PlayerPrefs.SetInt(CHECKPOINTKEY, curCheckPoint);
    }

    public void ResetCurCheckPoint()
    {
        curCheckPoint = 0;
        PlayerPrefs.SetInt(CHECKPOINTKEY, curCheckPoint);
    }

    public int GetCurCheckPoint()
    {
        if (PlayerPrefs.HasKey(CHECKPOINTKEY))
        {
            return PlayerPrefs.GetInt(CHECKPOINTKEY);
        }
        else
        {
            Debug.Log("No checkpoint saved");
            return 0;
        }
    }

    #endregion

    #region SAVINGLEVELDATA

    public void SaveCurLevel(string _levelName)
    {
        curScene = _levelName;
        PlayerPrefs.SetString(SCENEKEY, curScene);
    }

    public void EraseSavedLevel()
    {
        PlayerPrefs.DeleteKey(SCENEKEY);
    }

    public string GetCurSavedLevel()
    {
        if (PlayerPrefs.HasKey(SCENEKEY))
        {
            return PlayerPrefs.GetString(SCENEKEY);
        }
        else
        {
            string sceneName = SceneManager.GetSceneByBuildIndex(0).name;
            Debug.Log("No Scene Data found. Returning first scene " + sceneName);
            return sceneName;
        }
    }

    #endregion
}
