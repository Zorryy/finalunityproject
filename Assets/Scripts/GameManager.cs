﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    [SerializeField]
    private PlayerUI playerUI;
    [SerializeField]
    private PlayerData playerData;
    [SerializeField]
    private SpawnManager spawnManager;
    [SerializeField]
    private LevelManager levelManager;
    [SerializeField]
    private float killHeight = -20;

	// Use this for initialization
	void Awake ()
    {
        instance = this;
		
	}
	
    public PlayerData GetPlayerData()
    {
        return playerData;
    }

	public PlayerUI GetPlayerUI()
    {
        return playerUI;
    }

    public SpawnManager GetSpawnManager()
    {
        return spawnManager;
    }

    public LevelManager GetLevelManager()
    {
        return levelManager;
    }

    public float GetKillHeight()
    {
        return killHeight;
    }

}
