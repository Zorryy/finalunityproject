﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{

    [SerializeField]
    private LayerMask hitMask; 

    private float speed = 0;
    private int damage = 0;

    private Vector3 prevPos;

    [SerializeField]
    private float lifeTime = 3;
    private float lifeTimer;

    void Start()
    {
        lifeTimer = 0;
        prevPos = transform.position;
    }

    void Update()
    {
        KillTimer();
    }

    void FixedUpdate()
    {
        MoveBullet();
        CheckPassThrough();
    }

    void MoveBullet()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    void CheckPassThrough()
    {
        Vector2 movementThisStep = transform.position - prevPos;
        float rayLength = movementThisStep.sqrMagnitude;

        RaycastHit2D hit = Physics2D.Raycast(prevPos, movementThisStep, rayLength, hitMask);
        if (hit)
        {
            Enemy enemy = hit.collider.GetComponent<Enemy>();
            DamageEnemy(enemy);
        }

        prevPos = transform.position;
    }

    void KillTimer()
    {
        lifeTimer += Time.deltaTime;
        if (lifeTimer > lifeTime)
        {
            KillBullet();
        }
    }

    void DamageEnemy(Enemy _enemy)
    {
        _enemy.DamageHp(damage);
    }

    public void SetParams(int _damage, float _speed)
    {
        damage = _damage;
        speed = _speed;
    }

    void DoHitFX()
    {

    }

    void KillBullet()
    {
        Destroy(this.gameObject);
    }
	
}
