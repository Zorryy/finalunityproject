﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour 
{

    [SerializeField]
    private float speed;
    [SerializeField]
    private Transform leftPos;
    [SerializeField]
    private Transform rightPos;
    [SerializeField]
    private bool bounce;
    [SerializeField]
    private float bouncePower;
    [SerializeField]
    private LayerMask mask;


    private bool grounded;

    private Rigidbody2D rb;


	// Use this for initialization
	void Start () 
	{
        SetYRotation(180);

        //get components
        rb = GetComponent<Rigidbody2D>();
    }
	
    void Update()
    {
        CheckGrounded();
    }

	// Update is called once per frame
	void FixedUpdate () 
	{
        MoveEnemy();
        if (bounce)
        {
            BounceEnemy();
        }

    }

    void MoveEnemy()
    {
        //move enemy..always moves "right" based on rotation
        transform.Translate(Vector3.right * speed * Time.deltaTime);

        //rotate enemy based in waypoint x position
        if (transform.localPosition.x <= leftPos.localPosition.x)
        {
            SetYRotation(0);
        }
        else if (transform.localPosition.x >= rightPos.localPosition.x)
        {
            SetYRotation(180);
        }
    }

    void SetYRotation(float _yValue)
    {
        transform.localEulerAngles = new Vector3(0, _yValue, 0);
    }

    float GetYRotation()
    {
        return transform.localEulerAngles.y;
    }

    void CheckGrounded()
    {

        RaycastHit2D hit = Physics2D.Raycast((Vector2)transform.position + (Vector2.up * 0.1f),
            Vector2.down, Mathf.Infinity, mask);

        if (hit)
        {
            if (hit.distance < 0.11f)
                grounded = true;
            else
                grounded = false;
        }
        else
            grounded = false;
        
    }

    void BounceEnemy()
    {
        if (grounded)
        {
            rb.Sleep();
            rb.AddForce(Vector3.up * bouncePower, ForceMode2D.Impulse);
        }    
    }

    void OnCollisionEnter2D(Collision2D _col)
    {
        if (_col.collider.tag == "Enemy")
        {
            if (GetYRotation() > 1)
                SetYRotation(0);
            else
                SetYRotation(180);
        }
    }
}
