﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{

    [SerializeField]
    private int damage = 1;
    [SerializeField]
    private bool trigger = true;
    [SerializeField]
    private bool oneHitKill = false;
    [SerializeField]
    private bool bouncePlayer = true;
    [SerializeField]
    private float bounceForce = 5;

    void Start()
    {
        Collider2D col = GetComponent<Collider2D>();
        col.isTrigger = trigger;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            Player pl = col.gameObject.GetComponent<Player>();
            AddDamage(pl, col.contacts[0].point);
                
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.tag == "Player")
        {
            Player pl = col.GetComponent<Player>();
            AddDamage(pl,pl.transform.position);
        }
    }

    void AddDamage(Player _player, Vector2 _contactPoint)
    {
        if (oneHitKill)
            _player.DamageHp(_player.GetCurHp());
        else
        {
            if (bouncePlayer)
            {
                Vector2 direction = _contactPoint - (Vector2)transform.position;
                _player.DamageHp(damage, bounceForce, direction);

            }
            else
            {
                _player.DamageHp(damage);
            }
            
        }
            
    }


}
