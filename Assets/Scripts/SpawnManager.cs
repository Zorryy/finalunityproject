﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour 
{

    [SerializeField]
    private Transform[] checkPoints;
    [SerializeField]
    private bool progressOnly = true;
    [SerializeField]
    private bool resetLevelOnDeath = false;
    [SerializeField]
    private bool resetProgressOnQuit = true;
    [SerializeField]
    private GameObject playerSpawn;
    [SerializeField]
    private float respawnTime = 3;
    
    private int curCheckPoint;
    private Transform player;

    private LevelManager lm;
    private PlayerData pd;

    // Use this for initialization
    void Start () 
	{
        lm = GameManager.instance.GetLevelManager();
        pd = GameManager.instance.GetPlayerData();

        SetCheckPointIndexes();
        SpawnPlayer();
	}
	
    void SetCheckPointIndexes()
    {
        for (int i = 0; i < checkPoints.Length; i++)
        {
            checkPoints[i].GetComponent<CheckPoint>().SetCheckPointInd(i);
        }
    }

	public void SpawnPlayer()
    {
        if (resetLevelOnDeath)
        {
            curCheckPoint = lm.GetCurCheckPoint();
        }

        //store spawn in transform var for respawn use
        player = Instantiate(playerSpawn, checkPoints[curCheckPoint].position, 
            checkPoints[curCheckPoint].rotation).transform;
    }

    public IEnumerator RespawnPlayer()
    {
        yield return new WaitForSeconds(respawnTime);

        if (pd.GetCurLives() > 0)
        {
            if (resetLevelOnDeath)
                //reset level at checkpoint 
                ResetLevel(curCheckPoint);
            else
                ResetPlayerPosition();
        }
        else
            //reset entire level at first checkpoint if no lives
            ResetLevel(0);

       
    }

    void ResetPlayerPosition()
    {
        Debug.Log("resetting player position to " + checkPoints[curCheckPoint].position);
        player.position = checkPoints[curCheckPoint].position;

        //reset player health
        Player health = player.GetComponent<Player>();
        health.SetHealthDefaults();
    }

    void ResetLevel(int _checkPoint)
    {
        lm.SaveCurCheckPoint(_checkPoint);
        lm.ResetCurLevel();
    }

    public void SetCurCheckPoint(int _ind, bool _saveToDisc)
    {
        if (progressOnly)
        {
            if (_ind > curCheckPoint)
                curCheckPoint = _ind;
        }
        else
            curCheckPoint = _ind;
    }

    void OnApplicationQuit()
    {
        if (resetProgressOnQuit)
        {
            Debug.Log("Erasing Checkpoint progress for " + lm.GetCurLevelName());
            lm.ResetCurCheckPoint();
        }
    }

    
}
