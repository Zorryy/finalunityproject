﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallThrough : MonoBehaviour 
{

    private bool ignore;

    private bool down;

    [SerializeField]
    private float ignoreTime = 1;

	// Update is called once per frame
	void Update () 
	{
		if (Input.GetAxisRaw("Vertical") < 0 && !down)
            down = true;
        else if(down)
            down = false;

        if (down)
        {
            StartCoroutine(StartFall());
        }
	}

    void FallThroughPlatform(bool _fall)
    {
        if (_fall)
            Debug.Log("collisions deactivated");
        else
            Debug.Log("collisions activated");


        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("FallThrough"), _fall);
    }

    IEnumerator StartFall()
    {
        FallThroughPlatform(true);
        yield return new WaitForSeconds(ignoreTime);
        FallThroughPlatform(false);
    }
}
