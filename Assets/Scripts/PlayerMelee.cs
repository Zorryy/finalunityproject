﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMelee : MonoBehaviour 
{

    [SerializeField]
    private int damage;
    [SerializeField]
    private float hitDelay = 0.05f;
    private float timer;
    [SerializeField]
    private Transform meleePos;
    [SerializeField]
    private float hitRadius = 0.3f;
    [SerializeField]
    private LayerMask hitMask;

    private bool attack;
    private bool attacking;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
        GetInputs();
        CheckAttacking();
	}

    void GetInputs()
    {
        attack = Input.GetButtonDown("Fire1");

        if (attack)
        {
            if (!attacking)
                attacking = true;
        }
    }

    void CheckAttacking()
    {
        if (attacking)
        {
            timer += Time.deltaTime;
            if (timer > hitDelay)
            {
                CreateHitZone();
                attacking = false;
                timer = 0;
            }
        }
    }

    void CreateHitZone()
    {
        //grabs all colliders in the layer mask within that circle
        Collider2D[] cols = Physics2D.OverlapCircleAll(meleePos.position, hitRadius, hitMask);

        //cycle through the colliders..grabbing the enemies
        foreach (var col in cols)
        {
            //grab enemy health component
            Enemy enemy = col.GetComponent<Enemy>();

            if (enemy) // only applies damage if component is found
                //damage enemy
                DamageEnemy(enemy);
        }
    }

    void DamageEnemy(Enemy _enemy)
    {
        _enemy.DamageHp(damage);
    }
}
